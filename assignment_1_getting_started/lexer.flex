%top{
#define YYSTYPE int
}
%option noyywrap
%option yylineno
%x STRING
%x COMMENT
IDENTIFIER ([a-zA-Z_][a-zA-Z0-9_]*)
%%

[ \t\n]+ {} /*--------------PROCESS WHITESPACES-----------------*/

"{" {printf("LC: %s\n",yytext);}
"}" {printf("RC: %s\n",yytext);}
"[" {printf("LB: %s\n",yytext);}
"]" {printf("RB: %s\n",yytext);}
"(" {printf("LP: %s\n",yytext);}
")" {printf("RP: %s\n",yytext);}
";"              {printf("SEMICOLON: %s\n",yytext);}
"."              {printf("PERIOD: %s\n",yytext);}
","              {printf("COMMA: %s\n",yytext);}

"&&"              {printf("AND: %s\n",yytext);}

"="              {printf("EQUAL: %s\n",yytext);}
"+"              {printf("PLUS: %s\n",yytext);}
"-"              {printf("MINUS: %s\n",yytext);}
"*"              {printf("MULTIPLY: %s\n",yytext);}

"<"              {printf("LOWER_THAN: %s\n",yytext);}
">"              {printf("MORE_THAN: %s\n",yytext);}

"class" {printf("CLASS: %s\n",yytext);}
"new"              {printf("NEW: %s\n",yytext);}
"extends"              {printf("EXTENDS: %s\n",yytext);}
"length"              {printf("LENGTH: %s\n",yytext);}
"return"              {printf("RETURN: %s\n",yytext);}
"public"              {printf("PUBLIC: %s\n",yytext);}
"static"              {printf("STATIC: %s\n",yytext);}
"void"              {printf("VOID: %s\n",yytext);}
"main"              {printf("MAIN: %s\n",yytext);}
"String"              {printf("STRING: %s\n",yytext);}

"if"              {printf("IF: %s\n",yytext);}
"while"              {printf("WHILE: %s\n",yytext);}
"System.out.println"              {printf("PRINT: %s\n",yytext);}

"int"              {printf("INT: %s\n",yytext);}
"boolean"             {printf("BOOLEAN: %s\n",yytext);}

"true"              {printf("TRUE: %s\n",yytext);}
"false"              {printf("FALSE: %s\n",yytext);}
"this"              {printf("THIS: %s\n",yytext);}
"!"                 {printf("EX_MARK: %s\n",yytext);}

-?[0-9]+  {printf("INTEGER: %s\n",yytext);}
{IDENTIFIER}  {printf("IDENTIFIER: %s\n",yytext);}



"//"              {BEGIN COMMENT;}/*["]              {BEGIN STRING;}<STRING>["]      {BEGIN INITIAL;}<STRING>[^"]*   {printf("STRING: %s\n", yytext);}<STRING><<EOF>> {printf("Warning: Unclosed double-quote on line %d\n",yylineno); return 1;}*/
<COMMENT>[^($|"\n"|"\r"|"\r\n")]*              {printf("Blocked comment: %s\n",yytext);}
<COMMENT>[$|"\n"|"\r"|"\r\n"]      {BEGIN INITIAL;}

<<EOF>>   {printf("EOF"); return 1;}
%%